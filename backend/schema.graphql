# Copyright 2018 Vi Jay Suskind
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

schema {
	query: Query
	mutation: Mutation
}

type Query {
	# For debugging
	hello: String!

	# Check if user credentials exist in the database
	#
	# Returns true if user credentials match
	authenticateUser(username: String!, password: String!): Boolean!

	# Get all posts
	#
	# Returns list of posts
	getPosts: [Post]!

	# Get a particular post
	#
	# Returns a particular post
	getPostById(id: ID!): Post

	# Get all comments for a particular post at a particular depth
	#
	# Returns list of comments
	# getCommentsAtDepth(postId: ID!, depth: Int!): [Comment]!
}

type Mutation {
	# Create a user with the given username and password
	#
	# Returns true if successful, and false otherwise
	createUser(username: String!, password: String!): Boolean!

	# Delete a user with the given username after authenticating with password
	#
	# Returns true if successful, and false otherwise
	deleteUser(username: String!, password: String!): Boolean!

	# Change a user's password after authenticating with the old password
	#
	# Returns true if successful, and false otherwise
	changeUserPassword(oldpassword: String!, newpassword: String!): Boolean!

	# Check if user credentials exist in the database
	#
	# Returns the appropriate JSON Web Token
	getJWT(username: String!, password: String!): String!

	# Create a post by a certain user using JWT
	#
	# Returns the unique ID of the post
	createPostJWT(url: String!, title: String!): String!

	# Delete all posts by author
	#
	# Returns true if succeess
	# deletePostsByAuthor(author: String!): Boolean!

	# Upvote post using JWT username
	#
	# Returns true if success
	upvotePost(id: ID!): String!

	# Undo upvote of post using JWT username
	#
	# Returns true if success
	# downvotePost(id: ID!): Boolean!

	# Create a comment
	#
	# Returns the unique ID of the comment
	# createComment(postId: ID!, parent: ID, comment: String!): ID!
}

type Post {
	id: ID!
	author: String!
	url: String!
	title: String!
	score: Int!
}

type Comment {
	id: ID!
	author: String!
	comment: String!
	parent: ID
}
