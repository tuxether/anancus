// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"net/http"
	"time"

	"github.com/go-fed/activity/deliverer"
	"github.com/go-fed/activity/pub"
	"github.com/spf13/viper"
	"golang.org/x/time/rate"
)

// Clock determines the time.
type Clock struct{}

func (c *Clock) Now() time.Time {
	return time.Now()
}

// HttpClient sends http requests.
type HttpClient struct{}

func (m *HttpClient) Do(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	resp, err := client.Do(req)
	return resp, err
}

// Deliverer schedules federated ActivityPub messages for delivery, possibly
// asynchronously.
func createDeliverer() pub.Deliverer {
	opts := deliverer.DeliveryOptions{}
	opts.InitialRetryTime = 60 * time.Second
	opts.MaximumRetryTime = time.Hour
	opts.BackoffFactor = 1.5
	opts.MaxRetries = 30
	opts.RateLimit = rate.NewLimiter(0.5, 30)

	dpool := deliverer.NewDelivererPool(opts)

	return dpool
}

func createPubber() (pubber pub.Pubber, asHandler pub.HandlerFunc) {
	// pub.Clock
	clock := &Clock{}

	// pub.SocialFederateApplication
	app := &Application{}

	// pub.Callbacker
	socialCallbacker := &SocialCallbacker{}
	federatedCallbacker := &FederatedCallbacker{}

	// pub.Deliverer
	deliverer := createDeliverer()

	// pub.HttpClient
	httpClient := &HttpClient{}

	userAgent := viper.GetString("pubber.userAgent")
	maxDeliveryDepth := viper.GetInt("pubber.maxDeliveryDepth")
	maxInboxForwardingDepth := viper.GetInt("pubber.maxInboxForwardingDepth")

	pubber = pub.NewPubber(clock, app, socialCallbacker, federatedCallbacker, deliverer, httpClient, userAgent, maxDeliveryDepth, maxInboxForwardingDepth)
	asHandler = pub.ServeActivityPubObject(app, clock)
	return
}
