// Copyright 2018 Vi Jay Suskind
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-fed/activity/vocab"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/spf13/viper"
	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/ssh"
)

// Helpers
// =======

func ActivityPubRequest(r *http.Request) *http.Request {
	if r.Method == "POST" {
		existing, ok := r.Header["Content-Type"]
		if ok {
			r.Header["Content-Type"] = append(existing, "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
		} else {
			r.Header["Content-Type"] = []string{"application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""}
		}
	} else {
		existing, ok := r.Header["Accept"]
		if ok {
			r.Header["Accept"] = append(existing, "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
		} else {
			r.Header["Accept"] = []string{"application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""}
		}
	}
	r.Header["Date"] = []string{time.Now().UTC().Format("Mon, 02 Jan 2006 15:04:05") + " GMT"}
	return r
}

// Data Types
// ==========

type Resolver struct{}

type post struct {
	id     graphql.ID
	author string
	url    string
	title  string
	score  int32
}

type postResolver struct {
	p *post
}

func (p postResolver) ID() graphql.ID {
	return p.p.id
}

func (p postResolver) Author() string {
	return p.p.author
}

func (p postResolver) Url() string {
	return p.p.url
}

func (p postResolver) Title() string {
	return p.p.title
}

func (p postResolver) Score() int32 {
	return p.p.score
}

// Interfaces
// ==========

func (r *Resolver) Hello(ctx context.Context) (string, error) {
	return "Hello, World!", nil
}

func (r *Resolver) CreatePostJWT(ctx context.Context, p struct {
	Url   string
	Title string
}) (string, error) {
	var postAuthor string

	jwtSigningKey := viper.GetString("password.jwtSigningKey")
	mySigningKey := []byte(jwtSigningKey)

	// Grab the token string from the context
	token := ctx.Value("jwt").(*jwt.Token)
	if token != nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			postAuthor = claims["sub"].(string)
		}
	}

	doc := &vocab.Document{}
	doc.AppendNameString(p.Title)
	u, err := url.Parse(p.Url)
	if err != nil {
		log.Fatal("could not parse url:", err)
	}
	doc.AppendUrlAnyURI(u)

	m, err := doc.Serialize()
	if err != nil {
		log.Fatal("failed to serialize:", err)
	}
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatal("could not marshal json:", err)
	}
	m["@context"] = "https://www.w3.org/ns/activitystreams"
	postUrl := fmt.Sprintf("%s/activity/person/%s/outbox", baseUrl, postAuthor)
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(b))
	if err != nil {
		log.Fatal("could not post outbox:", err)
	}

	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		log.Fatal("could not get SignedString:", err)
	}

	req.Header.Set("Host", viper.GetString("server.hostname"))
	req.Header.Set("Authorization", "Bearer "+ss)
	req = ActivityPubRequest(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not get response:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}

	return string(dump), nil
}

func (r *Resolver) DeletePostsByAuthor(ctx context.Context, p struct{ Author string }) (bool, error) {
	sql := "UPDATE documents SET author='deleted' WHERE author=$1;"
	_, err := db.Exec(sql, p.Author)
	if err != nil {
		panic(err)
	}

	return true, nil
}

func (r *Resolver) UpvotePost(ctx context.Context, p struct {
	ID graphql.ID
}) (string, error) {
	var voter string

	jwtSigningKey := viper.GetString("password.jwtSigningKey")
	mySigningKey := []byte(jwtSigningKey)

	// Grab the token string from the context
	token := ctx.Value("jwt").(*jwt.Token)
	if token != nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			voter = claims["sub"].(string)
		}
	}

	like := &vocab.Like{}
	u, err := url.Parse(fmt.Sprintf("%s/activity/person/%s", baseUrl, voter))
	if err != nil {
		log.Println("failed to parse url:", err)
		return "", err
	}
	like.AppendActorIRI(u)
	pid, err := strconv.Atoi(string(p.ID))
	if err != nil {
		log.Println("failed to convert graphql.ID to int:", err)
	}
	u, err = url.Parse(fmt.Sprintf("%s/document/%d", baseUrl, pid))
	if err != nil {
		log.Println("failed to parse url:", err)
		return "", err
	}
	like.AppendObjectIRI(u)

	m, err := like.Serialize()
	if err != nil {
		log.Fatal("failed to serialize:", err)
	}
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatal("could not marshal json:", err)
	}
	m["@context"] = "https://www.w3.org/ns/activitystreams"
	postUrl := fmt.Sprintf("%s/activity/person/%s/outbox", baseUrl, voter)
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(b))
	if err != nil {
		log.Fatal("could not post outbox:", err)
	}

	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		log.Fatal("could not get SignedString:", err)
	}

	req.Header.Set("Host", viper.GetString("server.hostname"))
	req.Header.Set("Authorization", "Bearer "+ss)
	req = ActivityPubRequest(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not get response:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}

	return string(dump), nil

	// var count int

	// sql := "SELECT COUNT(*) FROM liked WHERE id=$1 AND author=$2;"
	// err := db.QueryRow(sql, p.ID, voter).Scan(&count)
	// if err != nil {
	// 	panic(err)
	// }

	// if count > 0 {
	// 	sql = "DELETE FROM liked WHERE id=$1 AND author=$2;"
	// 	_, err := db.Exec(sql, p.ID, voter)
	// 	if err != nil {
	// 		return false, err
	// 	}
	// }

	// sql = "INSERT INTO liked (id, author) VALUES ($1, $2);"
	// _, err = db.Exec(sql, p.ID, voter)
	// if err != nil {
	// 	return false, err
	// }

	// return true, nil
}

func (r *Resolver) DownvotePost(ctx context.Context, p struct {
	ID graphql.ID
}) (bool, error) {
	var voter string

	// Grab the token string from the context
	token := ctx.Value("jwt").(*jwt.Token)
	if token != nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			voter = claims["sub"].(string)
		}
	}

	sql := "DELETE FROM liked WHERE id=$1 AND author=$2;"
	_, err := db.Exec(sql, p.ID, voter)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *Resolver) GetPostById(ctx context.Context, p struct {
	ID graphql.ID
}) (*postResolver, error) {
	var returnval *postResolver

	var (
		id     int
		author string
		url    string
		title  string
		score  int
	)

	sql := "SELECT id,author,url,name FROM documents WHERE id=$1;"
	err := db.QueryRow(sql, p.ID).Scan(&id, &author, &url, &title)
	if err != nil {
		panic(err)
	}

	sql = "SELECT COUNT(*) FROM liked WHERE uri=$1;"
	err = db.QueryRow(sql, id).Scan(&score)
	if err != nil {
		panic(err)
	}

	gid := graphql.ID(strconv.Itoa(id))
	gscore := int32(score)

	post := &post{
		id:     gid,
		author: author,
		url:    url,
		title:  title,
		score:  gscore,
	}
	returnval = &postResolver{post}

	return returnval, nil
}

func (r *Resolver) GetPosts(ctx context.Context) ([]*postResolver, error) {
	var returnval []*postResolver

	var (
		id     int
		author string
		url    string
		title  string
		score  int
	)

	sql := "SELECT id,author,url,name FROM documents ORDER BY created_at DESC;"
	rows, err := db.Query(sql)
	defer rows.Close()
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		err = rows.Scan(&id, &author, &url, &title)
		if err != nil {
			panic(err)
		}

		sql := "SELECT COUNT(*) FROM liked WHERE uri=$1;"
		err = db.QueryRow(sql, id).Scan(&score)
		if err != nil {
			panic(err)
		}

		gid := graphql.ID(strconv.Itoa(id))
		gscore := int32(score)

		post := &post{
			id:     gid,
			author: author,
			url:    url,
			title:  title,
			score:  gscore,
		}
		returnval = append(returnval, &postResolver{post})
	}

	return returnval, nil
}

func (r *Resolver) CreateUser(ctx context.Context, u struct {
	Username string
	Password string
}) (bool, error) {
	argonTime := uint32(viper.GetInt("password.time"))
	argonMemory := uint32(viper.GetInt("password.memory"))
	argonThreads := uint8(viper.GetInt("password.threads"))
	argonKeyLen := uint32(viper.GetInt("password.keyLength"))
	argonSaltLen := viper.GetInt("password.saltLength")

	salt := make([]byte, argonSaltLen)
	_, err := rand.Read(salt)
	if err != nil {
		return false, err
	}

	key := argon2.IDKey([]byte(u.Password), salt, argonTime, argonMemory, argonThreads, argonKeyLen)
	sKey := base64.StdEncoding.EncodeToString(append(salt, key...))

	// TODO: ED25519 Marshal support isn't upstream yet. Switch to ED25519
	//       once support has been upstreamed.

	rsaKey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return false, err
	}

	privateKey := string(pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(rsaKey),
	}))

	publicPart, err := ssh.NewPublicKey(&rsaKey.PublicKey)
	if err != nil {
		return false, err
	}

	publicKey := string(ssh.MarshalAuthorizedKey(publicPart))

	fingerprint := strings.TrimPrefix(ssh.FingerprintSHA256(publicPart), "SHA256:")

	sql := "INSERT INTO persons (name, password, public_key, private_key, fingerprint) VALUES ($1, $2, $3, $4, $5);"
	_, err = db.Exec(sql, u.Username, sKey, publicKey, privateKey, fingerprint)
	if err != nil {
		return false, err
	}

	return true, nil
}

func canAuthenticate(username, password string) bool {
	argonTime := uint32(viper.GetInt("password.time"))
	argonMemory := uint32(viper.GetInt("password.memory"))
	argonThreads := uint8(viper.GetInt("password.threads"))
	argonKeyLen := uint32(viper.GetInt("password.keyLength"))
	argonSaltLen := viper.GetInt("password.saltLength")

	sql := "SELECT password FROM persons WHERE name=$1;"
	var sKey string
	err := db.QueryRow(sql, username).Scan(&sKey)
	if err != nil {
		panic(err)
	}

	tmp, err := base64.StdEncoding.DecodeString(sKey)
	if err != nil {
		panic(err)
	}
	salt := tmp[:argonSaltLen]
	key1 := tmp[argonSaltLen:]

	key2 := argon2.IDKey([]byte(password), salt, argonTime, argonMemory, argonThreads, argonKeyLen)

	return bytes.Equal(key1, key2)
}

func (r *Resolver) DeleteUser(ctx context.Context, u struct {
	Username string
	Password string
}) (bool, error) {
	if !canAuthenticate(u.Username, u.Password) {
		return false, errors.New("unable to authenticate")
	}

	sql := "DELETE FROM persons WHERE name=$1;"
	_, err := db.Exec(sql, u.Username)
	if err != nil {
		panic(err)
	}
	return true, nil
}

func (r *Resolver) GetJWT(ctx context.Context, u struct {
	Username string
	Password string
}) (string, error) {
	jwtSigningKey := viper.GetString("password.jwtSigningKey")

	if !canAuthenticate(u.Username, u.Password) {
		return "", errors.New("unable to authenticate")
	}

	mySigningKey := []byte(jwtSigningKey)
	// Create the Claims
	claims := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(24 * time.Hour).Unix(),
		Subject:   u.Username,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(mySigningKey)
	return ss, err
}

func (r *Resolver) AuthenticateUser(ctx context.Context, u struct {
	Username string
	Password string
}) (bool, error) {
	if !canAuthenticate(u.Username, u.Password) {
		return false, errors.New("unable to authenticate")
	}
	return true, nil
}

func (r *Resolver) ChangeUserPassword(ctx context.Context, u struct {
	Username    string
	OldPassword string
	NewPassword string
}) (bool, error) {
	argonTime := uint32(viper.GetInt("password.time"))
	argonMemory := uint32(viper.GetInt("password.memory"))
	argonThreads := uint8(viper.GetInt("password.threads"))
	argonKeyLen := uint32(viper.GetInt("password.keyLength"))
	argonSaltLen := viper.GetInt("password.saltLength")

	if !canAuthenticate(u.Username, u.OldPassword) {
		return false, errors.New("unable to authenticate")
	}

	var author string

	// Grab the token string from the context
	token := ctx.Value("jwt").(*jwt.Token)
	if token != nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			author = claims["sub"].(string)
		}
	}

	newSalt := make([]byte, argonSaltLen)
	_, err := rand.Read(newSalt)
	if err != nil {
		return false, errors.New("Error reading random salt")
	}
	newKey := argon2.IDKey([]byte(u.NewPassword), newSalt, argonTime, argonMemory, argonThreads, argonKeyLen)
	newSKey := base64.StdEncoding.EncodeToString(append(newSalt, newKey...))
	sql := "UPDATE persons SET password=$2 WHERE name=$1;"
	_, err = db.Exec(sql, author, newSKey)
	if err != nil {
		panic(err)
	}
	return true, nil
}
