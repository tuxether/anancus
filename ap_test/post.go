package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/go-fed/activity/vocab"
)

func main() {
	// doc := &vocab.Document{}
	// u, err := url.Parse("https://floss.social")
	// if err != nil {
	// 	log.Fatalf("could not parse URL:", err)
	// }
	// doc.AppendUrlAnyURI(u)
	// doc.AppendNameString("Mastodon")
	// m, err := doc.Serialize()

	like := &vocab.Like{}
	actor, err := url.Parse("http://localhost:8000/activity/person/jess")
	if err != nil {
		log.Fatalf("could not parse actor url:", err)
	}
	like.AppendActorIRI(actor)

	// doc := &vocab.Document{}
	u, err := url.Parse("https://floss.social")
	if err != nil {
		log.Fatalf("could not parse URL:", err)
	}
	// doc.AppendUrlAnyURI(u)
	// doc.AppendNameString("Mastodon")
	// like.AppendObject(doc)

	like.AppendObjectIRI(u)

	to, err := url.Parse("http://localhost:8000/activity/person/jess/followers")
	if err != nil {
		log.Fatalf("could not parse followers url:", err)
	}
	like.AppendToIRI(to)

	cc, err := url.Parse("http://localhost:8000/activity/person/james")
	if err != nil {
		log.Fatalf("could not parse cc url:", err)
	}
	like.AppendCcIRI(cc)

	m, err := like.Serialize()
	if err != nil {
		log.Fatalf("failed to serialize:", err)
	}
	m["@context"] = "https://www.w3.org/ns/activitystreams"
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("could not marshal json:", err)
	}
	fmt.Println(string(b))
	postUrl := "http://localhost:8000/activity/person/jess/outbox"
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(b))
	if err != nil {
		log.Fatalf("could not post outbox:", err)
	}
	req.Header.Set("Host", "localhost")
	req.Header.Set("Authorization", "Bearer XXXXXXXXXXX")
	req.Header.Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not get response:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}
	fmt.Println(string(dump))
}
