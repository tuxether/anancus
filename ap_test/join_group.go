package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/go-fed/activity/vocab"
)

func main() {
	join := &vocab.Join{}
	u, err := url.Parse("http://:8000/activity/person/jess")
	if err != nil {
		log.Fatal("could not parse url", err)
	}
	join.AppendActorIRI(u)

	u, err = url.Parse("http://:8000/activity/group/golang")
	if err != nil {
		log.Fatal("could not parse url", err)
	}
	join.AppendObjectIRI(u)

	m, err := join.Serialize()
	if err != nil {
		log.Fatalf("failed to serialize:", err)
	}
	m["@context"] = "https://www.w3.org/ns/activitystreams"
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("could not marshal json:", err)
	}
	fmt.Println(string(b))
	postUrl := "http://localhost:8000/activity/person/jess/outbox"
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(b))
	if err != nil {
		log.Fatalf("could not post outbox:", err)
	}
	req.Header.Set("Host", "localhost")
	req.Header.Set("Authorization", "Bearer XXXXXXXXXXX")
	req.Header.Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not get response:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}
	fmt.Println(string(dump))
}
