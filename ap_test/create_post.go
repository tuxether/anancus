package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/go-fed/activity/vocab"
)

func main() {
	doc := &vocab.Document{}
	doc.AppendNameString("ActivityPub")
	u, err := url.Parse("https://www.w3.org/TR/activitypub/")
	if err != nil {
		log.Fatal("could not parse url", err)
	}
	doc.AppendUrlAnyURI(u)

	u, err = url.Parse("https://www.w3.org/ns/activitystreams#Public")
	if err != nil {
		log.Fatal("could not parse url", err)
	}
	doc.AppendToIRI(u)

	// u, err = url.Parse("http://100.115.94.204:8000/activity/person/jess")
	u, err = url.Parse("http://localhost:8000/activity/person/jess")
	if err != nil {
		log.Fatal("could not parse url", err)
	}
	doc.AppendCcIRI(u)

	m, err := doc.Serialize()
	if err != nil {
		log.Fatalf("failed to serialize:", err)
	}
	m["@context"] = "https://www.w3.org/ns/activitystreams"
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("could not marshal json:", err)
	}
	fmt.Println(string(b))
	// postUrl := "http://localhost:8000/activity/person/jess/outbox"
	postUrl := "http://localhost:8001/activity/person/james/outbox"
	// postUrl := "http://100.115.92.204:8001/activity/person/james/outbox"
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(b))
	if err != nil {
		log.Fatalf("could not post outbox:", err)
	}
	req.Header.Set("Host", "100.115.92.204")
	req.Header.Set("Authorization", "Bearer XXXXXXXXXXX")
	req.Header.Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("could not get response:", err)
	}
	defer resp.Body.Close()
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Fatalf("could not parse response body:", err)
	}
	fmt.Println(string(dump))
}
